# iTop Auto ACnSvcs API


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ricardoortiz.salud/itop-auto-acnsvcs-api.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ricardoortiz.salud/itop-auto-acnsvcs-api/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Alcance:

La Integración AC Create & Srv Update automatiza el proceso de creación de Acuerdos de Servicios a partir de la creación de una Organización por parte del usuario. Posteriormente, asigna una lista de servicios a esta organización. Esto asegura que la organización recién creada esté lista para ser utilizada en iTop sin necesidad de mayor intervención del usuario.

Para llevar a cabo este proceso, la integración realiza las siguientes acciones:

Ac Create:

Cuando se crea una organización en iTop, la integración genera automáticamente un Acuerdo de Servicio con la nomenclatura "Ac_" seguido del "Nombre de la Organización". Luego, se le asigna a este Acuerdo una lista de servicios heredados de la organización padre.

Services Update:

Si se modifica el campo "Modelo de Entrega" de una organización, la integración verifica si existe un Acuerdo de Servicio para esa organización. En caso de que no exista, procede a realizar el proceso de Ac Create. Si ya existe un Acuerdo de Servicios, se procede a asignarle la lista de servicios heredados de la organización padre.

Esta integración agiliza el proceso de configuración y asignación de servicios a las organizaciones en iTop, reduciendo la intervención manual del usuario y garantizando una operatividad más eficiente.

# 1.	Configuración Servidor de iTop:

1-	Instalar  Ac Create & Sevice Update Api en servidor de iTop:

Copiar archivo: intadservices.php
Ruta destino: [ruta del proyecto]/data/html/pages/

2-	Posicionarse en la ruta [ruta del proyecto]/data/html/pages/ y crear archivo de configuración con el comando:

`touch .env`

Otorgarle permisos con:

`chmod 604 .env`

3-	Editarlo y copiarle los datos de configuración del apartado 3 de este manual. 
4-	Agregar excepción de proxy en servidor de iTop:

    verificar variables de entorno en el contenedor:

```
docker ps -a
docker exec -it 'id del contenedor' env | grep -i proxy
```
    Agregar excepción del proxy:

```
nano [ruta del proyecto]/.env
(Agregar la url del servidor de iTop a la variable de entorno NO_PROXY)
cd [ruta del proyecto]
docker-compose down && docker-compose up -d
```


5-	Agregar excepción de proxy en variables de entorno propias del servidor.

# 2. Configuración Front iTop:

## Creación de usuario local iTop:

Usuario: iTopRest 
Password: [Establezca Contraseña segura]
Perfil: Administrator, REST Services User
Creación de conexión de aplicación remota:
Nombre: AutoAcService
Tipo de aplicación: Other / Generic
Ambiente: Producción
URL: https://[urliTop]/pages/intadservices.php

## Creación de Webhooks:

## Auto Ac & Services:
Información General
Nombre
    Crea Ac y Servicios
Descripción
    Crea un Acuerdo de Servicio y asigna Servicios al mismo
Estatus
    Activo
Idioma
    ES CR

## Conexión Webhook
Conexión
    AutoAcService
Conexión de prueba
    AutoAcService
Método
    POST
Path

Encabezados
    Content-type: application/json
Parámetros de Solicitud

Carga

```
    {   
        "params": {
        "org_id": "$this->id$",
        "org_name": "$this->name$",
        "parent_id":"$this->parent_id$",
        "provider_id":"7",
        "contracttype_id":"1"
        }
    }
```

## Donde:

provider_id: Es el id del proveedor de servicios por defecto, es del tipo "Organización".
contracttype_id: Es el id del tipo de contrato por defecto, de no existir ninguno debe crearse antes de la implementación.


# Creación de Disparadores:


## Disparador (creación de objeto): Ac y Services Autocreate
Descripción
    Ac y Services Autocreate
Context
Clase destino
    Organización

Acciones: 
Crea Ac y Servicios

## Disparador (actualizando un objecto): Actualiza Servicios
Descripción
    Actualiza Servicios
Context
Clase destino
    Organización
Filtro
    Campos objetivo
        deliverymodel_id
Acciones: 
    Crea Ac y Servicios

# 3. Configuración Middle API:

Completar el archivo de configuración “.env” con los siguientes datos:

iTopApiUrl=[URLiTop]/webservices/rest.php?version=1.3
iTopUser=[Usuario_de_servicio_iTop]
iTopPass=[Password] 
Donde:
URLiTop: Es la raíz del dominio donde está instalado iTop.
iTopUser: Es el usuario local generado en iTop para la utilización de la API/Res de iTop.
iTopPass: Es la clave de acceso a iTop correspondiente al usuario anterior.

