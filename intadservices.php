<?php


$configFile = file_get_contents('/var/www/html/pages/.env');
$configLines = explode(PHP_EOL, $configFile);
$config = [];
//$status="-";

//echo "Hostname Zabbix,ZabbixID,Hostname iTop,iTopID,Status"; echo "<BR>";

foreach($configLines as $line) { 

if (strpos($line, '=') !== false) {
	list($key, $value) = explode('=', $line, 2);
	$config[$key] = $value;
}
}
$iTopApiUrl = $config['iTopApiUrl'];
$iTopUser = $config['iTopUser'];
$iTopPass = $config['iTopPass'];

$json = file_get_contents('php://input');

if(!Valida_json($json)){exit("Json inválido \n");}

$jsondecode=json_decode($json,true);

$org_id=$jsondecode["params"]["org_id"];
$org_name=$jsondecode["params"]["org_name"];
$parent_id=$jsondecode["params"]["parent_id"];

$provider_id=$jsondecode["params"]["provider_id"];
$contracttype_id=$jsondecode["params"]["contracttype_id"];

//$services_list=$jsondecode["params"]["services_list"];
//$services_list=GetServiceList($parent_id);

//echo "provider_id : ".$provider_id."\n";
//echo "contracttype_id : ".$contracttype_id."\n";
//$srv_list=json_encode($services_list);

//echo "services list : ".$srv_list."\n";


$json_data='{
  "operation": "core/get",
  "class": "CustomerContract",
  "comment": "Obtener acuerdos de servicio de una organización",
  "key": "SELECT CustomerContract WHERE org_id = \''.$org_id.'\'"
}';

$response=Call_api_iTop($json_data);

//echo "Respuesta GET: ".$response."\n";

$data = json_decode($response, true);

if (isset($data['objects']) && is_array($data['objects']) && !empty($data['objects']) && 
isset($data['objects'][key($data['objects'])]['key']))
{

$provider_id=$data['objects'][key($data['objects'])]['fields']['provider_id'];
$contracttype_id=$data['objects'][key($data['objects'])]['fields']['contracttype_id'];


	if  (empty($data['objects'][key($data['objects'])]['fields']['services_list']))
	{
		$key = reset($data['objects'])['key'];
		$response= AcServicesUpdate($key);
	} else {
		echo "Acuerdos con clientes y servicios existentes";
	}

} else {

//echo "Crear AC: ";
//echo "OrgId: ".$org_id."\n";
//echo "Orgname: ".$org_name."\n";

$json_data='{
  "operation": "core/get",
  "class": "CustomerContract",
  "comment": "Obtener acuerdos de servicio de una organización padre",
  "key": "SELECT CustomerContract WHERE org_id = \''.$parent_id.'\'"
}';

$response=Call_api_iTop($json_data);
$data = json_decode($response, true);

if (isset($data['objects']) && is_array($data['objects']) && !empty($data['objects']) &&
isset($data['objects'][key($data['objects'])]['key'])) //si existe AC...
{
        $provider_id=$data['objects'][key($data['objects'])]['fields']['provider_id'];
        $contracttype_id=$data['objects'][key($data['objects'])]['fields']['contracttype_id'];
}


$response=AcCreate($org_id, $org_name);

}

function GetServicesList($parent_id)

{
//GLOBAL $srv_list;
GLOBAL $provider_id, $contracttype_id;

$json_data='{
  "operation": "core/get",
  "class": "CustomerContract",
  "comment": "Obtener acuerdos de servicio de una organización padre",
  "key": "SELECT CustomerContract WHERE org_id = \''.$parent_id.'\'"
}';

$response=Call_api_iTop($json_data);
$data = json_decode($response, true);

if (isset($data['objects']) && is_array($data['objects']) && !empty($data['objects']) &&
isset($data['objects'][key($data['objects'])]['key'])) //si existe AC...
{
        $provider_id=$data['objects'][key($data['objects'])]['fields']['provider_id'];
	$contracttype_id=$data['objects'][key($data['objects'])]['fields']['contracttype_id'];

        if  (!empty($data['objects'][key($data['objects'])]['fields']['services_list'])) //si existe servicios...
        {
                $services_list=$data['objects'][key($data['objects'])]['fields']['services_list'];
		$srv_list=json_encode($services_list);
//echo "srv List inicial: ".$srv_list."\n";
		$data = json_decode($srv_list, true);
		$new_array = array();
		foreach($data as $item) {
			$new_array[] = array(
			'service_id' => $item['service_id'],
			'sla_id' => $item['sla_id']);
		}

		$srv_list = json_encode($new_array);
//echo "srv List final: ".$srv_list."\n";

        } else { //si no existe servicios...
                echo "Servicios Padre inexistentes \n";
        }
} else { //si no existe AC
echo "Acuredo Padre inexistente \n";
}
return $srv_list;
}


function AcServicesUpdate($ac_id)

{

GLOBAL $parent_id;

$srv_list=GetServicesList($parent_id);

if  (!empty($srv_list)){

$json_data='{

  "operation": "core/update",
  "class": "CustomerContract",
  "comment": "Asociación de servicios a un acuerdo de servicio existente",
  "key": "'.$ac_id.'",
  "fields":{
    "services_list":'.$srv_list.',
    "status": "production"
  }}';


//echo "json srv update".$json_data."\n";

$response=Call_api_iTop($json_data);

//echo "response update service".$response."\n";

$data = json_decode($response, true);
if (isset($data['objects']) && is_array($data['objects']) && !empty($data['objects'])) {

        $name=$data["objects"][array_keys($data["objects"])[0]]["fields"]["name"];
        echo "Servicios actualizados en: ".$name."\n";

}else{

//echo "Response Update: ".$response."\n";

}

return $response;
}else{
echo "Servicios no fueron actualizados \n";
}

}

function AcCreate($org_id,$org_name)

{

GLOBAL $provider_id, $contracttype_id;

$ac_name = "Ac_".$org_name;

//echo "org_name: ".$org_name."\n";
//echo "ac_name: ".$ac_name."\n";
//echo "org_id: ".$org_id."\n";

//echo "provider_id : ".$provider_id."\n";
//echo "contracttype_id : ".$contracttype_id."\n";


$json_data='{

  "operation": "core/create",
  "class": "CustomerContract",
  "comment": "Creación de un nuevo acuerdo de servicio para una organización",
  "fields": {
  "name": "'.$ac_name.'",
  "status":"production",
  "provider_id":"'.$provider_id.'",
  "description": "Descripción del Acuerdo",
  "org_id": "'.$org_id.'",
  "contracttype_id":"'.$contracttype_id.'"

  }
}';

$response=Call_api_iTop($json_data);

$data = json_decode($response, true);

if (isset($data['objects']) && is_array($data['objects']) && !empty($data['objects'])) {

        $key = reset($data['objects'])['key'];

        echo "Creado Acuerdo con cliente: ".$ac_name." ID: ".$key."\n";

        $response= AcServicesUpdate($key);



}else{

//echo "Response Create: ".$response."\n";

}
return $response;

}





function Call_api_iTop($apijson) { GLOBAL $iTopApiUrl,$iTopUser,$iTopPass; $curl = curl_init(); 
                curl_setopt_array($curl, array(
                  CURLOPT_URL => $iTopApiUrl, CURLOPT_RETURNTRANSFER => true, CURLOPT_ENCODING => '', 
                  CURLOPT_MAXREDIRS => 10, CURLOPT_TIMEOUT => 0, CURLOPT_FOLLOWLOCATION => true, 
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => 'POST', 
                  CURLOPT_POSTFIELDS => array('json_data' => $apijson,'auth_user' => $iTopUser,'auth_pwd' => 
                  $iTopPass),
                )); $response = curl_exec($curl); curl_close($curl); 

return $response;
}

function Valida_json($json)

{
$data = json_decode($json, true);
if ($data !== null && isset($data['params']) && is_array($data['params'])) 
{
return true;
} else {
return false;
}
}


?>
